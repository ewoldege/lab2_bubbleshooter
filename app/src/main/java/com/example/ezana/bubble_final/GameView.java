package com.example.ezana.bubble_final;

//EZANA WOLDEGEBRIEL
//MOHAMED GHANY
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import java.util.Random;
import android.transition.TransitionManager;
import android.view.ViewGroup;

public class GameView extends SurfaceView implements SurfaceHolder.Callback,View.OnTouchListener {

    public static int restart=0;
    public static RigidBall          ball;
    public static RigidBall[][]     balls;
    private GameThread  bst;
    public static int Colors;
    private int TotalBall;
    public static int nballrow;
    public static int nballcol;
    private int maxncol;
    int fcount=0;
    int scount = 0;
    int action_down=0;
    static int xtouch=0;
    static int ytouch=0;
    static int clicked=0;
    static int alphacounter=0;
    public static int counter=0;
    public GameView(int width,int height,Context context) {

        super(context);
        // Notify the SurfaceHolder that you'd like to receive SurfaceHolder callbacks.
        getHolder().addCallback(this);
        setOnTouchListener(this);

        // If we need a fixed radius, use this command and comment the line below
        //nballrow = height/(2*ball.pixelradius);
       nballrow = 12;//Fixes the amount of balls per row.
       nballcol = 5;//Fixes the amount of balls per column
        ball.pixelradius = height/(2*nballrow); // comment this line if we want to fix the radius
        maxncol = width/(2*ball.pixelradius);//grid size length

        TotalBall = nballcol*nballrow;


        // this is the view on which you will listen for touch events
        ball = new RigidBall(this, Color.GREEN); //Constructs the moving ball

        //These are the colors that will be associated with each difficulty level. Easy will have colors YELLOW GREEN RED BLUE; HARD WILL HAVE easy colors plus
        //MAGENTA AND GRAY. Hard colors will also have colors light gray and an offshade of purple (i ran out of colors so i made one up!)
        if (MainActivity.diffFlag == 0)
            Colors = 4;
        else if (MainActivity.diffFlag == 1)
            Colors = 6;
        else
            Colors = 8;

        balls = new RigidBall[maxncol][nballrow];//Creates an array of size TotalBalls which will be the balls that will be printed onto the screen.
//All balls in the original ball array are set to the color white
        for(int vcount=0;vcount<maxncol;vcount++)
            for(int ccount=0;ccount<nballrow; ccount++)
                balls[vcount][ccount]=new RigidBall(this,Color.WHITE);


        for (int t = 0; t < nballcol; t++) {
            for (int r = 0; r < nballrow; r++) {//This loop makes sure that all the bubbles that will be printed at the top of the screen have zero velocity.
                //Look at the RigidBall.java to see the actual code for StationaryBall();

                balls[t][r].StationaryBall();
            }
        }



    }

    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub
        xtouch = (int) event.getX();
        ytouch = (int) event.getY();

        if(event.getAction()== MotionEvent.ACTION_DOWN){
            action_down=1;
        }
        else if(event.getAction()==MotionEvent.ACTION_UP) {
            clicked = 1;
            action_down=0;

        }
        return true;
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        renderGame(canvas);
    }

    protected void renderGame(Canvas c) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        c.drawPaint(paint); // The code above basically creates and sets the white background.
//The 20 lines or so below this is what we used to implement the fading effect when a ball is being deleted. We used alpha animation to make this happen.
        if(RigidBall.deletion==1){
            for(int di=0;di<nballcol;di++){
                for(int dj=0;dj<nballrow;dj++){
                    if(balls[di][dj].tobedeleted==1){
                        balls[di][dj].drawMany(c,balls[di][dj].mX, balls[di][dj].mY,255-alphacounter*15);

                    }
                }
            }
            alphacounter++;
            if(alphacounter>17){
                RigidBall.deletion=0;
                alphacounter=0;
                for(int mi=0;mi<nballcol;mi++){
                    for(int mj=0;mj<nballrow;mj++){
                        if(balls[mi][mj].tobedeleted==1) {
                            balls[mi][mj].tobedeleted = 0;
                            balls[mi][mj].mColor=Color.WHITE;
                        }

                    }
                }
            }
        }
        //draws the crosshairs of where you are aiming.
        if(action_down==1) {
            Paint paint_line = new Paint();
            paint_line.setStyle(Paint.Style.FILL);
            paint_line.setColor(Color.BLACK);
            c.drawLine(0,ytouch,getWidth(),ytouch,paint_line);
            c.drawLine(xtouch,0,xtouch,getHeight(),paint_line);

        }

        if (clicked == 0)
            ball.StationaryBall();//if the screen is not touched, the shooting ball does not move.
        else {
            if (counter == 0) {
                ball.MovingBall();//if the screen is touched the shooting ball is allowed to move.
                counter++;
            }
        }

        ball.draw(c); //This draws the moving ball onto the screen
//Assigns colors to the balls. This happens only once per game!
        if (fcount == 0) {
            for (int xcount = 0; xcount < nballcol; xcount++) {
                for (int ycount = 0; ycount < nballrow; ycount++) {

                    Random rand = new Random();
                    int randomNum = rand.nextInt((Colors));

                    switch (randomNum){
                        case 0:    balls[xcount][ycount].mColor = Color.YELLOW;
                            break;
                        case 1:    balls[xcount][ycount].mColor = Color.GREEN;
                            break;
                        case 2:     balls[xcount][ycount].mColor = Color.RED;
                            break;
                        case 3:     balls[xcount][ycount].mColor = Color.BLUE;
                            break;
                        case 4:     balls[xcount][ycount].mColor = Color.MAGENTA;
                            break;
                        case 5:     balls[xcount][ycount].mColor = Color.DKGRAY;
                            break;
                        case 6:     balls[xcount][ycount].mColor = Color.LTGRAY;
                            break;
                        case 7:     balls[xcount][ycount].mColor = Color.rgb(101, 30, 69);
                            break;
                    }
                    fcount++;

                }
            }
        }

        // This nested for loops is to set the neighbors of each ball with their colors

        //This nested if/for loop compilation is what prints the balls onto the screen. Only ones marked "not deleted" will be printed.
        //This if/for loop also checks to see which balls have filled neighbors and which ones are free. It also records the neighbors colors!
        for (int jcount = 0; jcount < nballcol; jcount++) {
            for (int ncount = 0; ncount < nballrow; ncount++) {
                if (balls[jcount][ncount].mColor != Color.WHITE) {
                    if(balls[jcount][ncount].deleted==0&&balls[jcount][ncount].tobedeleted==0) {


                        balls[jcount][ncount].mX = ((2 * (ncount + 1) - 1) * balls[jcount][ncount].mR);
                        balls[jcount][ncount].mY = (2 * (jcount + 1) - 1) * balls[jcount][ncount].mR;
                        balls[jcount][ncount].drawMany(c, balls[jcount][ncount].mX, balls[jcount][ncount].mY,255);

                        if (ncount != 0 && jcount != 0 && ncount != (nballrow - 1)) {
                            balls[jcount][ncount].nb[0] = balls[jcount - 1][ncount].mColor;
                            balls[jcount][ncount].nb[1] = balls[jcount][ncount + 1].mColor;
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[3] = balls[jcount][ncount - 1].mColor;

                        } else if (ncount == 0 && jcount == 0) {
                            balls[jcount][ncount].nb[1] = balls[jcount][ncount + 1].mColor;
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[0] = Color.WHITE;
                            balls[jcount][ncount].nb[3] = Color.WHITE;
                        } else if (ncount == (nballrow - 1) && jcount == 0) {
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[3] = balls[jcount][ncount - 1].mColor;
                            balls[jcount][ncount].nb[1] = Color.WHITE;
                            balls[jcount][ncount].nb[0] = Color.WHITE;

                        } else if (ncount == 0) {
                            balls[jcount][ncount].nb[0] = balls[jcount - 1][ncount].mColor;
                            balls[jcount][ncount].nb[1] = balls[jcount][ncount + 1].mColor;
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[3] = Color.WHITE;
                        } else if (ncount == (nballrow - 1)) {
                            balls[jcount][ncount].nb[0] = balls[jcount - 1][ncount].mColor;
                            balls[jcount][ncount].nb[1] = Color.WHITE;
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[3] = balls[jcount][ncount - 1].mColor;
                        } else if (jcount == 0) {
                            balls[jcount][ncount].nb[1] = Color.WHITE;
                            balls[jcount][ncount].nb[1] = balls[jcount][ncount + 1].mColor;
                            balls[jcount][ncount].nb[2] = balls[jcount + 1][ncount].mColor;
                            balls[jcount][ncount].nb[3] = balls[jcount][ncount - 1].mColor;
                        }
                        for (int w = 0; w < 4; w++) {
                            if (balls[jcount][ncount].nb[w] == Color.WHITE) {
                                balls[jcount][ncount].free = 1;
                            }
                        }


                    }
                }
            }
        }
        
        //The code below helps us find when to reset the game. The idea is that when all of the original balls in the game
        //have been deleted, then the game will end (even if there happened to be balls placed below the spot where the original
        //ones were placed
        int countl=0;
        for(int w=0;w<nballcol;w++){
            for(int d=0;d<nballrow;d++){
                if(balls[w][d].deleted==1) {


                    countl++;
                }
            }
        }
        if(countl==(nballcol*nballrow)) {
            ((Activity) getContext()).finish();

        }
        //TransitionManager.beginDelayedTransition(View v);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        bst = new GameThread(this);
        bst.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // The cleanest way to stop a thread is by interrupting it.
        // The thread must regularly check its interrupt flag.
        bst.interrupt();
    }
}
