package com.example.ezana.bubble_final;

//EZANA WOLDEGEBRIEL
//MOHAMED GHANY

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Ahmed on 3/24/2015.
 */
public class GameThread extends Thread {
    static GameView bsv;

    public GameThread(GameView bsv) {
        this.bsv= bsv;
    }

    @Override
    public void run() {
        SurfaceHolder sh = bsv.getHolder();

        while( !Thread.interrupted() ) {
            Canvas c = sh.lockCanvas(null);
            try {
                synchronized(sh) {
                    bsv.renderGame(c);
                }
            } catch (Exception e) {
                System.err.print( e.getStackTrace() );
            } finally {
                if (c!=null) {
                    sh.unlockCanvasAndPost(c);
                }
            }
            // Set the frame rate by setting this delay
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                // This means that this thread was interrupted while sleeping.
                return;
            }
        }
    }
}
