package com.example.ezana.bubble_final;

//EZANA WOLDEGEBRIEL
//MOHAMED GHANY

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import java.util.Random;

public class RigidBall {

    // Class properties
    public int visited;
    static Resources res = Resources.getSystem();
    public static int Radius = 20;
    public static int deletion=0;
    public static int singleBallDeletion=0;
    static int pixelradius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Radius,
            res.getDisplayMetrics());
    public final int RADIUS       = pixelradius;
    public static final  int x       = (MainActivity.width)/2;
    public static final  int y       = (MainActivity.height);
    private static final int MIN_VELOCITY = 20;
    private static final int MAX_VELOCITY = 50;
    public int[] nb = new int[4];

    static int ColorMatchFlag=0;


    // Object properties
    private final GameView  bwv;
    public final int              mR;
    public  int              mColor;
    private       int              mVelX;
    public        int              mVelY;
    public         int              mX;
    public        int              mY;
    public        int              free;
    public        int              deleted;
    public        int              tobedeleted;

    public RigidBall( GameView bwv, int COLOR ) {
        Random rng  = new Random();
        this.mVelX  = rng.nextInt(MAX_VELOCITY - MIN_VELOCITY + 1) + MIN_VELOCITY;
        this.mVelY  = rng.nextInt(MAX_VELOCITY - MIN_VELOCITY + 1) + MIN_VELOCITY;
        this.bwv    = bwv;
        this.mColor = COLOR;
        this.mR     = RADIUS;
        this.mX     = x;
        this.mY     = y;
        this.visited = 0;
        this.free   =0;
        this.deleted =0;
        this.tobedeleted=0;
        this.nb[0] = 0;
        this.nb[1] = 0;
        this.nb[2] = 0;
        this.nb[3] = 0;

    }

    //Draws the moving ball
    public void draw( Canvas c ) {
        Paint paint = new Paint();
        paint.setColor( mColor );
        c.drawCircle(this.getX(), this.getY(), this.getR(), paint);
        this.stepCoordinates();
    }
    //Draws the stationary balls at the top of the screen
    public void drawMany( Canvas c,int xcoor, int ycoor,int alpha ) {

        Paint paint = new Paint();
        paint.setColor(mColor);
        paint.setAlpha(alpha);
        c.drawCircle(xcoor, ycoor, this.getR(), paint);

    }
    //Makes the ball which calls upon it stationary
    public void StationaryBall(){
        this.mVelX = 0;
        this.mVelY = 0;
    }
    //Calculates where and how fast the moving ball moves throughout the view.
    public void MovingBall(){
        int newXcoor;
        int newYcoor;
        double angle;
        newXcoor=(GameView.xtouch-x);
        newYcoor=(y-(GameView.ytouch));
        if(newXcoor>=-40&&newXcoor<=40)
        {
            this.mVelX=0;
            this.mVelY=40;
        }
        else{
            angle = Math.atan(newYcoor/newXcoor);
            this.mVelX = (int)(40*(Math.cos(angle)));
            this.mVelY = (int)(40*(Math.sin(angle)));
            if(newXcoor<0)
                this.mVelX=-this.mVelX;
        }
//Vector Analysis
        /*else {
            if (Math.abs(newXcoor) > Math.abs(newYcoor)) {
                if (newXcoor > 0)
                    this.mVelY = (int) (Math.sqrt((40 * 40) / (1 + Math.pow(Math.abs(newXcoor) / newYcoor, 2))));
                else
                    this.mVelY = -(int) (Math.sqrt((40 * 40) / (1 + Math.pow(Math.abs(newXcoor) / newYcoor, 2))));
                this.mVelX = (newXcoor / newYcoor) * mVelY;
            } else if (Math.abs(newXcoor) < Math.abs(newYcoor)) {
                if (newXcoor > 0)
                    this.mVelX = (int) (Math.sqrt((40 * 40) / (1 + Math.pow(Math.abs(newYcoor) / newXcoor, 2))));
                else
                    this.mVelX = -(int) (Math.sqrt((40 * 40) / (1 + Math.pow(Math.abs(newYcoor) / (newXcoor + 0.001), 2))));
                this.mVelY = (int) (newYcoor / (newXcoor + 0.001)) * mVelX;
            } else if (Math.abs(newXcoor) == Math.abs(newYcoor)) {
                this.mVelX = (int) (40 / (Math.sqrt(2)));
                this.mVelY = mVelX;
            }
        }*/
    }
    //Returns a distance between two points.
    public double getDistance(int x1, int y1, int x2, int y2){
        return Math.sqrt((Math.pow(x2-x1,2)+Math.pow(y2-y1,2)));
    }
    //Adds a ball onto the grid. Helpful for when the moving ball latches onto an open grid coordinate.
    public static void ballAdd(RigidBall balladd){
        balladd.mX = GameView.ball.mX;
        balladd.mY = GameView.ball.mY;
        balladd.mColor = GameView.ball.mColor;
        balladd.deleted=0;
    }
    //After the ball latches onto an open grid coordinate it resets its parameters and assigns a random color. It also
    //is ready to shoot again.
    public static void resetBall(){
        Random rand = new Random();
        int randomNum = rand.nextInt((GameView.Colors));

        switch (randomNum){
            case 0:    GameView.ball.mColor = Color.YELLOW;
                break;
            case 1:    GameView.ball.mColor = Color.GREEN;
                break;
            case 2:     GameView.ball.mColor = Color.RED;
                break;
            case 3:     GameView.ball.mColor = Color.BLUE;
                break;
            case 4:     GameView.ball.mColor = Color.MAGENTA;
                break;
            case 5:     GameView.ball.mColor = Color.DKGRAY;
                break;
            case 6:     GameView.ball.mColor = Color.LTGRAY;
                break;
            case 7:     GameView.ball.mColor = Color.rgb(101, 30, 69);
                break;
        }
        GameView.ball.mX = x;
        GameView.ball.mY = y;
        GameView.counter=0;
        GameView.clicked=0;
    }

    //This function checks where the moving ball makes contact with the stationary balls and checks to see which
    //neighbor it should be assigned to. It uses several instances of the getdistance() method in order to find out
    //which neighboring cell is the closest.
    public void ballPlacement(){

        for(int i=0;i<GameView.nballcol;i++) {
            for (int j = 0; j < GameView.nballrow; j++) {
                if(GameView.balls[i][j].free==1&&GameView.balls[i][j].deleted==0) {
                    if ((getDistance(mX, mY, GameView.balls[i][j].mX, GameView.balls[i][j].mY)) < 2 * pixelradius) {
                        this.StationaryBall();
                        double[] dis = new double[4];
                        double smallestDistance;
                        double nextDistance;
                        double temp;
                        dis[0] = getDistance(mX, mY, GameView.balls[i][j].mX - 2 * pixelradius, GameView.balls[i][j].mY);
                        dis[1] = getDistance(mX, mY, GameView.balls[i][j].mX, GameView.balls[i][j].mY - 2 * pixelradius);
                        dis[2] = getDistance(mX, mY, GameView.balls[i][j].mX + 2 * pixelradius, GameView.balls[i][j].mY);
                        dis[3] = getDistance(mX, mY, GameView.balls[i][j].mX, GameView.balls[i][j].mY + 2 * pixelradius);
                        smallestDistance = dis[0];
                        nextDistance = dis[1];

                        for (int k = 0; k < 4; k++) {
                            if(smallestDistance > nextDistance)
                            {
                                temp = nextDistance;
                                nextDistance = smallestDistance;
                                smallestDistance=temp;
                            }
                            if (smallestDistance > dis[k])
                                smallestDistance = dis[k];
                            else
                            if(nextDistance > dis[k])
                                nextDistance = dis[k];
                        }
                        if (smallestDistance == dis[0]) {

                            if(GameView.balls[i][j-1].mColor!=Color.WHITE){
                                if(nextDistance==dis[1])
                                    collisionResponse(GameView.balls,1,i,j);
                                else if(nextDistance==dis[2])
                                    collisionResponse(GameView.balls,2,i,j);
                                else if(nextDistance==dis[3]) {
                                    collisionResponse(GameView.balls, 3, i, j);

                                }

                            }
                            else
                                collisionResponse(GameView.balls,0,i,j);
                        } else if (smallestDistance == dis[1]) {

                            if(GameView.balls[i-1][j].mColor!=Color.WHITE){
                                if(nextDistance==dis[0])
                                    collisionResponse(GameView.balls,0,i,j);
                                else if(nextDistance==dis[2])
                                    collisionResponse(GameView.balls,2,i,j);
                                else if(nextDistance==dis[3]) {
                                    collisionResponse(GameView.balls, 3, i, j);

                                }

                            }
                            else
                                collisionResponse(GameView.balls,1,i,j);
                        } else if (smallestDistance == dis[2]) {

                            if(GameView.balls[i][j+1].mColor!=Color.WHITE){
                                if(nextDistance==dis[1])
                                    collisionResponse(GameView.balls,1,i,j);
                                else if(nextDistance==dis[0])
                                    collisionResponse(GameView.balls,0,i,j);
                                else if(nextDistance==dis[3]) {
                                    collisionResponse(GameView.balls, 3, i, j);

                                }

                            }
                            else
                                collisionResponse(GameView.balls,2,i,j);
                        } else if (smallestDistance == dis[3]) {

                            if(GameView.balls[i+1][j].mColor!=Color.WHITE){
                                if(nextDistance==dis[1])
                                    collisionResponse(GameView.balls,1,i,j);
                                else if(nextDistance==dis[2])
                                    collisionResponse(GameView.balls,2,i,j);
                                else if(nextDistance==dis[0])
                                    collisionResponse(GameView.balls,0,i,j);

                            }
                            else {
                                collisionResponse(GameView.balls, 3, i, j);

                            }
                        }
                        break;

                    }
                }

            }
        }
//for the case where the ball is about to hit the top border of the screen, we also made the ball stop and latch onto
        //the nearest neighboring cell.
        if(mY < pixelradius) {
            this.StationaryBall();
            int flag=0;
            double[] disy = new double[GameView.nballrow];
            double smallestDistance;
            double nextDistance;
            double temp;

            for(int l=0;l<GameView.nballrow;l++){
                disy[l]=getDistance(mX,mY,((2*l+1)*pixelradius),mY);
            }
            smallestDistance = disy[0];
            nextDistance = disy[1];

            for (int k = 0; k < GameView.nballrow; k++) {
                if (smallestDistance > nextDistance) {
                    temp = nextDistance;
                    nextDistance = smallestDistance;
                    smallestDistance = temp;
                }
                if (smallestDistance > disy[k])
                    smallestDistance = disy[k];
                else if (nextDistance > disy[k])
                    nextDistance = disy[k];
            }
            for(int h=0;h<GameView.nballrow;h++){

                if(flag==0&&smallestDistance==disy[h]){
                    GameView.ball.mX = ((2*h+1)*pixelradius);
                    GameView.ball.mY = pixelradius;
                    ballAdd(GameView.balls[0][h]);
                    ColorMatch(GameView.balls,0,h);
                    resetBall();
                    flag++;
                }

            }
        }


    }

    //This function assigns the stationary shooting ball onto its designated landing position and adds it onto the
    //array of stationary balls. It is then checked for matching colors with its neighbors using the ColorMatch method.
    //After any matches are made, the moving ball is reset to its standard position at the bottom of the screen and the game
    //resumes.
    public static void collisionResponse(RigidBall[][] ballss, int number,int i2,int j2){

        if (number==0){
            GameView.ball.mX = ballss[i2][j2].mX - 2 * pixelradius;
            GameView.ball.mY = ballss[i2][j2].mY;
            ballAdd(ballss[i2][j2-1]);

            ColorMatch(ballss, i2, j2 - 1);

        }
        else if(number==1){
            GameView.ball.mX = ballss[i2][j2].mX;
            GameView.ball.mY = ballss[i2][j2].mY - 2 * pixelradius;
            ballAdd(ballss[i2-1][j2]);
            ColorMatch(ballss,i2-1,j2);

        }
        else if(number==2){
            GameView.ball.mX = ballss[i2][j2].mX + 2 * pixelradius;
            GameView.ball.mY = ballss[i2][j2].mY;
            ballAdd(ballss[i2][j2+1]);
            ColorMatch(ballss,i2,j2+1);

        }
        else if(number==3){
            GameView.ball.mX = ballss[i2][j2].mX;
            GameView.ball.mY = ballss[i2][j2].mY + 2 * pixelradius;
            ballAdd(ballss[i2+1][j2]);
            if((i2+1)>(GameView.nballcol-1))
                GameView.nballcol++;

            ColorMatch(ballss,i2+1,j2);

        }
        resetBall();

    }
    //This function and colormatch2 both accomplish the same thing. This method initializes the ColorMatch checks and also has
    //a way to delete the balls at the end (by calling the deleteBalls() method in order to get delete an array of balls set for
    //deletion. ColorMatch2 recursively checks for matching neighbors throughout the grid until it cannot find any more matching neighbors
    //as it checks to see which matches are made, if there is a match those balls are added to the deletedballs array and are marked
    //for deletion.
    public static void ColorMatch(RigidBall[][] balls1,int i1, int j1){
        int h[]=new int[1];
        RigidBall[] deletedBalls = new RigidBall[200];
        ColorMatchFlag=0;





        if (i1 != 0) {
            if (balls1[i1 - 1][j1].visited == 0 && balls1[i1][j1].mColor == balls1[i1 - 1][j1].mColor) {

                balls1[i1 - 1][j1].visited = 1;
                balls1[i1][j1].visited = 1;
                ColorMatchFlag++;
                ColorMatch2(balls1, i1 - 1, j1, deletedBalls, h);
                if (ColorMatchFlag == 1) {
                    balls1[i1 - 1][j1].visited = 0;
                    balls1[i1][j1].visited = 0;
                }

                deletedBalls[h[0]] = balls1[i1 - 1][j1];
                h[0]++;

            }
        }
        if (j1 != 0) {
            if (balls1[i1][j1 - 1].visited == 0 && balls1[i1][j1].mColor == balls1[i1][j1 - 1].mColor) {
                balls1[i1][j1 - 1].visited = 1;
                balls1[i1][j1].visited = 1;
                ColorMatch2(balls1, i1, j1 - 1, deletedBalls, h);
                ColorMatchFlag++;
                if (ColorMatchFlag == 1) {
                    balls1[i1][j1 - 1].visited = 0;
                    balls1[i1][j1].visited = 0;
                }

                deletedBalls[h[0]] = balls1[i1][j1 - 1];
                h[0]++;
            }
        }
        if (j1 != (GameView.nballrow - 1)) {
            if (balls1[i1][j1 + 1].visited == 0 && balls1[i1][j1].mColor == balls1[i1][j1 + 1].mColor) {
                balls1[i1][j1 + 1].visited = 1;
                balls1[i1][j1].visited = 1;
                ColorMatch2(balls1, i1, j1 + 1, deletedBalls, h);
                ColorMatchFlag++;

                if (ColorMatchFlag == 1) {
                    balls1[i1][j1 + 1].visited = 0;
                    balls1[i1][j1].visited = 0;
                }
                deletedBalls[h[0]] = balls1[i1][j1 + 1];
                h[0]++;
            }
        }
        if (balls1[i1 + 1][j1].visited == 0 && balls1[i1][j1].mColor == balls1[i1 + 1][j1].mColor) {
            ColorMatchFlag++;
            balls1[i1 + 1][j1].visited = 1;
            balls1[i1][j1].visited = 1;
            ColorMatch2(balls1, i1 + 1, j1, deletedBalls, h);

            if (ColorMatchFlag == 1) {
                balls1[i1 + 1][j1].visited = 0;
                balls1[i1][j1].visited = 0;
            }
            deletedBalls[h[0]] = balls1[i1 + 1][j1];
            h[0]++;
        }

        if (ColorMatchFlag >= 2) {
            //balls1[i1][j1].mColor = Color.WHITE;
            balls1[i1][j1].tobedeleted=1;
            balls1[i1][j1].deleted = 1;
            balls1[i1][j1].visited = 0;
            deleteBalls(deletedBalls, h);
        }

        if(i1!=0&&(balls1[i1-1][j1].deleted==1||balls1[i1-1][j1].mColor==Color.WHITE))
            deleteBall(balls1[i1][j1]);

    }

    public static void ColorMatch2(RigidBall[][] balls1,int i1,int j1,RigidBall[] deletedBalls,int h[]){
        if(i1!=0) {
            if (balls1[i1-1][j1].visited==0&&balls1[i1][j1].mColor == balls1[i1 - 1][j1].mColor) {

                ColorMatchFlag++;
                balls1[i1-1][j1].visited=1;
                balls1[i1][j1].visited=1;


                ColorMatch2(balls1, i1 - 1, j1,deletedBalls,h);

                deletedBalls[h[0]]=balls1[i1-1][j1];
                h[0]++;

            }
        }
        if(j1!=0) {
            if (balls1[i1][j1-1].visited==0&&balls1[i1][j1].mColor == balls1[i1][j1 - 1].mColor) {
                ColorMatchFlag++;
                balls1[i1][j1-1].visited=1;
                balls1[i1][j1].visited=1;
                ColorMatch2(balls1, i1, j1 - 1,deletedBalls,h);


                deletedBalls[h[0]]=balls1[i1][j1-1];
                h[0]++;
            }
        }
        if(j1!=(GameView.nballrow-1)) {
            if (balls1[i1][j1+1].visited==0&&balls1[i1][j1].mColor == balls1[i1][j1 + 1].mColor) {
                ColorMatchFlag++;
                balls1[i1][j1+1].visited=1;
                balls1[i1][j1].visited=1;
                ColorMatch2(balls1, i1, j1 + 1,deletedBalls,h);


                deletedBalls[h[0]]=balls1[i1][j1+1];
                h[0]++;
            }
        }
        if (balls1[i1+1][j1].visited==0&&balls1[i1][j1].mColor == balls1[i1 + 1][j1].mColor) {
            ColorMatchFlag++;

            balls1[i1+1][j1].visited=1;
            balls1[i1][j1].visited=1;
            ColorMatch2(balls1, i1 + 1, j1,deletedBalls,h);

            deletedBalls[h[0]]=balls1[i1+1][j1];
            h[0]++;
        }
    }

    //The deleteBalls method is used to delete an array of balls that were marked for deletion by the colormatch functions
    //shown above.
    public static void deleteBalls(RigidBall[] ballss, int h[]){
        int k=1;

        for(int i=0; i<h[0]; i++){

            deletion=1;
            // ballss[i].mColor=Color.WHITE;
            ballss[i].tobedeleted=1;
            ballss[i].deleted=1;
            ballss[i].visited=0;
        }
        for(int y=1;y<GameView.nballcol;y++){
            for(int x=0;x<(GameView.nballrow);x++){
                do {
                    if (GameView.balls[y - 1][x].deleted == 1 && GameView.balls[y][x].deleted==0) {
                        // GameView.balls[y][x].mColor = Color.WHITE;
                        GameView.balls[y][x].tobedeleted=1;
                        GameView.balls[y][x].deleted = 1;
                        GameView.balls[y][x].visited = 0;
                        k=1;
                    }
                    else
                        k=0;
                } while(k==1);
            }
        }
    }
    //This function deletes a single ball which is more likely than not going to be the movingball that latches onto a target.
    public static void deleteBall(RigidBall ball){
        deletion=1;
        //ball.mColor=Color.WHITE;
        ball.tobedeleted=1;
        ball.deleted=1;
        ball.visited=0;
    }
    public int getX() { return mX; }
    public int getY() { return mY; }
    public int getR() { return mR; }

    // This function below sets the rules of the bouncing ball across the boundaries of the view.
    //Most of this function was used from the discussion code provided by Ahmed, but some revisions were made by
    //Ezana and Mohamed in order to fit our program.
    public void stepCoordinates() {
        int maxX = bwv.getWidth();
        int maxY = bwv.getHeight();
        this.mX += mVelX;
        this.mY += mVelY;

        if ( mX > ( maxX - mR) ) {
            mVelX = -mVelX;
            mX = maxX - mR;
        } else if ( mX < mR) {
            mVelX = -mVelX;
            mX = mR;
        }
        if ( mY > ( maxY - mR) ) {
            mVelY = -mVelY;
            mY = maxY - mR;
        } else if ( mY < mR) {
            // mVelY = -mVelY;
            // mY = mR;
            mVelX=0;
            mVelY=0;
        }

        ballPlacement();
    }
}
