package com.example.ezana.bubble_final;

//EZANA WOLDEGEBRIEL
//MOHAMED GHANY

import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.graphics.Color;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.util.Log;


//MAIN MENU ACTIVITY
public class MainActivity extends ActionBarActivity {
    public static int width;
    public static int height;
    public static int diffFlag=1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        //new Layout Type
        RelativeLayout WelcomeScreen = new RelativeLayout(this);
        WelcomeScreen.setBackgroundColor(Color.CYAN);

        //Create some buttons for difficulty level
        Button easy = new Button(this);
        easy.setText("EASY");
        easy.setBackgroundColor(Color.WHITE);

        Button medium = new Button(this);
        medium.setText("MEDIUM");
        medium.setBackgroundColor(Color.YELLOW);

        Button hard = new Button(this);
        hard.setText("HARD");
        hard.setBackgroundColor(Color.RED);


        //Giving IDs to the buttons
        easy.setId(1);
        medium.setId(2);
        hard.setId(3);

        //Grabbing the dimensions
        RelativeLayout.LayoutParams easyButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );

        RelativeLayout.LayoutParams mediumButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );


        RelativeLayout.LayoutParams hardButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );

        //Setting the placement rules. Easy button will be aligned in the center, but will be above medium
        //Hard will also be in the center but will be below medium
        //They both also have some space in between the buttons; hence, the need for margins.
        easyButton.addRule(RelativeLayout.CENTER_HORIZONTAL);
        easyButton.addRule(RelativeLayout.ABOVE,medium.getId());
        easyButton.setMargins(0,0,0,50);

        mediumButton.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mediumButton.addRule(RelativeLayout.CENTER_VERTICAL);

        hardButton.addRule(RelativeLayout.CENTER_HORIZONTAL);
        hardButton.addRule(RelativeLayout.BELOW,medium.getId());
        hardButton.setMargins(0,50,0,0);

        //I used this tool in order to make this display the same across any device.
        Resources res = getResources();
        int pixelsW = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,200,
                res.getDisplayMetrics());
        int pixelsH = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,75,
                res.getDisplayMetrics());
        int pixelradius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25,
                res.getDisplayMetrics());


        easy.setWidth(pixelsW);
        medium.setWidth(pixelsW);
        hard.setWidth(pixelsW);

        easy.setHeight(pixelsH);
        medium.setHeight(pixelsH);
        hard.setHeight(pixelsH);

        //Added buttons along with their accompanying design rules to the main view.
        WelcomeScreen.addView(easy,easyButton);
        WelcomeScreen.addView(medium,mediumButton);
        WelcomeScreen.addView(hard,hardButton);

        //Set this view as the main view for this activity.
        setContentView(WelcomeScreen);

        //EVENT HANDLING
        easy.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        diffFlag = 0;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );

        medium.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        diffFlag = 1;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );

        hard.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        diffFlag = 2;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
